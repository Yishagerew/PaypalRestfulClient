package com.medlanes.servers;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
@ApplicationPath("com.medlanes")
public class App_Config extends ResourceConfig{
public App_Config()
{
	packages("com.medlanes");
}
}
