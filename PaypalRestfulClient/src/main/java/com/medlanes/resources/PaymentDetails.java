package com.medlanes.resources;
import java.io.InputStream;

import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.paypal.core.rest.PayPalRESTException;
import com.paypal.core.rest.PayPalResource;
@Path("/paypalService")
public class PaymentDetails {
@Context
Request request;
@Context
UriInfo uriInfo;
private static final Logger LOGGER = Logger .getLogger(PaymentDetails.class);
static
{
InputStream is = PaymentDetails.class .getResourceAsStream("/sdk_config.properties"); 
try 
{ 
	PayPalResource.initConfig(is); 
	} 
catch (PayPalRESTException e) { 
	LOGGER.fatal(e.getMessage());
}
}

}
